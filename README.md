hmt-base - haskell music theory base
------------------------------------

base library for [haskell](http://haskell.org/) music theory

related:

- [hmt](http://rohandrape.net/?t=hmt)
- [hmt-diagrams](http://rohandrape.net/?t=hmt-diagrams)
- [hmt-texts](http://rohandrape.net/?t=hmt-texts)

## cli

[db](http://rohandrape.net/?t=hmt-base&e=md/db.md),
[gl](http://rohandrape.net/?t=hmt-base&e=md/gl.md),
[gr-planar](http://rohandrape.net/?t=hmt-base&e=md/gr-planar.md),
[obj](http://rohandrape.net/?t=hmt-base&e=md/obj.md),
[ply](http://rohandrape.net/?t=hmt-base&e=md/ply.md)

© [rohan drape](http://rohandrape.net/), 2006-2025, [gpl](http://gnu.org/copyleft/).

* * *

```
$ make doctest
Examples: 1268  Tried: 1268  Errors: 0  Failures: 0
$
```
