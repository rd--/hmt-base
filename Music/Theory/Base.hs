-- | Base (composite module)
module Music.Theory.Base (module M) where

import Music.Theory.Array as M
import Music.Theory.Array.Cell_Ref as M
import Music.Theory.Array.Csv as M
import Music.Theory.Array.Square as M
import Music.Theory.Array.Text as M
import Music.Theory.Bits as M
import Music.Theory.Bool as M
import Music.Theory.Braille as M
import Music.Theory.Byte as M
import Music.Theory.Colour as M
import Music.Theory.Combinations as M
import Music.Theory.Concurrent as M
import Music.Theory.Directory as M
import Music.Theory.Directory.Find as M
import Music.Theory.Either as M
import Music.Theory.Enum as M
import Music.Theory.Function as M
import Music.Theory.Geometry as M -- composite
import Music.Theory.Graph as M -- composite
import Music.Theory.Image.Svg as M
import Music.Theory.Io as M
import Music.Theory.Json as M
import Music.Theory.List as M
import Music.Theory.Map as M
import Music.Theory.Math as M
import Music.Theory.Math.Constant as M
import Music.Theory.Math.Convert as M
import Music.Theory.Math.Histogram as M
import Music.Theory.Math.Nichomachus as M
import Music.Theory.Math.Oeis as M
import Music.Theory.Math.Prime as M
import Music.Theory.Math.Prime.List as M
import Music.Theory.Maybe as M
import Music.Theory.Monad as M
import Music.Theory.Opt as M
import Music.Theory.Ord as M
import Music.Theory.Permutations as M
import Music.Theory.Permutations.List as M
import Music.Theory.Read as M
import Music.Theory.Set.List as M
import Music.Theory.Set.Set as M
import Music.Theory.Show as M
import Music.Theory.String as M
import Music.Theory.Time.Duration as M
import Music.Theory.Time.Notation as M
import Music.Theory.Traversable as M
import Music.Theory.Tree as M
import Music.Theory.Tuple as M
import Music.Theory.Unicode as M
