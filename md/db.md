# db

Plain text databases.

~~~~
$ hmt-db stat plain ~/ut/inland/db/artists.text
("#-records",98)
("#-keys",8)
("key-set","N K U C W E R Z")
$ hmt-db convert plain csv ~/ut/inland/db/artists.text /tmp/db.csv
$
~~~~
